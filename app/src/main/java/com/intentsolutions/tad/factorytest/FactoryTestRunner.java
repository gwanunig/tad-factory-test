package com.intentsolutions.tad.factorytest;

import android.os.AsyncTask;
import android.util.Log;

import com.intentsolutions.tad.factorytest.messages.LedRedResponse;
import com.intentsolutions.tad.factorytest.messages.Response;
import com.squareup.otto.Subscribe;

public class FactoryTestRunner {
	private static final String TAG = "FactoryTestRunner";

	/*
		 1) Prompt for initial output
		 2) Execute the test
		 3) Prompt for results
		 4) If success, next test.  If fail, exit.
		 */
	// FactoryTest is a command pattern class.  Sort of.

	int mCurrentTestIndex = 0;
	FactoryTest[] mFactoryTests = null;

	private FactoryTestRunnerListener mListener;

	public FactoryTestRunner(FactoryTestRunnerListener listener) {
		setListener(listener);
	}

	public boolean runTests(FactoryTest... tests) {
		if (tests == null || tests.length == 0) {
			return false;
		}

		if (mListener == null) {
			// TODO: LOG why.  Can't do tests if you're not listening people...
			return false;
		}

		mFactoryTests = tests;

		mListener.onTestReady(this, mFactoryTests[mCurrentTestIndex]);

		return true;
	}

	public void setListener(FactoryTestRunnerListener listener) {
		mListener = listener;
	}

	/**
	 * Have to prompt to start the next test because it requires some input on the user's behalf.
	 */
	public void startTest() {
		runTest(mFactoryTests[mCurrentTestIndex]);
	}

	private void runTest(FactoryTest test) {
		final FactoryTest mTest = test;

		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... objects) {
				// Test can NOT block and await some other event.  We don't care, it's outside
				// of the test runner's purview.
				mTest.executeTest();
				return null;
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				mListener.onTestStart(mTest);
			}

			@Override
			protected void onPostExecute(Void aVoid) {
				mListener.onTestComplete(FactoryTestRunner.this, mFactoryTests[mCurrentTestIndex]);
			}
		}.execute();
	}

	public void moveToNextTest() {
		mCurrentTestIndex++;

		if (mCurrentTestIndex == mFactoryTests.length) {
			mListener.onEnd(this);
		}

		mListener.onTestReady(this, mFactoryTests[mCurrentTestIndex]);
	}

	@Subscribe
	public void onResponse(Response response) {
		// Do something with the result...
		Log.d(TAG, "Received response of " + response.getResult());


	}
}
