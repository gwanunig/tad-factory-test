package com.intentsolutions.tad.factorytest;

public class FactoryTest {
	private final String mInitialPrompt;
	private final String mResultsPrompt;
	private final String mPromptTitle;
	private final Runnable mTestToExecute;

	public FactoryTest(String promptTitle, String initialPrompt, String resultsPrompt, Runnable testToExecute) {
		mInitialPrompt = initialPrompt;
		mResultsPrompt = resultsPrompt;
		mTestToExecute = testToExecute;
		mPromptTitle = promptTitle;
	}

	protected String getPromptTitle() {
		return mPromptTitle;
	}

	protected String getInitialPrompt() {
		return mInitialPrompt;
	}

	protected String getResultsPrompt() {
		return mResultsPrompt;
	}

	protected void executeTest() {
		mTestToExecute.run();
	}
}