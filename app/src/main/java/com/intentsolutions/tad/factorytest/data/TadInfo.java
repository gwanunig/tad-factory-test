package com.intentsolutions.tad.factorytest.data;

import android.bluetooth.BluetoothDevice;

/**
 * Created by tjmercer on 6/28/16.
 */
public class TadInfo {
    public String TadName;
    public BluetoothDevice TadDevice;
}
