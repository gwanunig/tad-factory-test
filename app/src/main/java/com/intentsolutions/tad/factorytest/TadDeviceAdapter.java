package com.intentsolutions.tad.factorytest;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.intentsolutions.tad.factorytest.data.TadInfo;

import java.util.ArrayList;
import java.util.List;

import static com.intentsolutions.tad.factorytest.R.id.info_text;

public class TadDeviceAdapter extends RecyclerView.Adapter<TadDeviceAdapter.TadDeviceViewHolder> {

    private ArrayList<TadInfo> mItems;

    public TadDeviceAdapter(ArrayList<TadInfo> items) {
        this.mItems = items;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public TadDeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.recycler_cell, parent, false);

        return new TadDeviceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TadDeviceViewHolder holder, int position) {
        //Bind the item to the views via the holder
        TadInfo tad = mItems.get(position);
        holder.vName.setText(tad.TadName);
        holder.device = tad.TadDevice;
    }

    public static class TadDeviceViewHolder extends RecyclerView.ViewHolder {
        protected TextView vName;
        protected BluetoothDevice device;
        public TadDeviceViewHolder(View v) {
            super(v);
            vName =  (TextView) v.findViewById(info_text);
        }
    }

    public static class RecyclerClickListener implements RecyclerView.OnItemTouchListener {

        private OnItemClickListener mListener;
        GestureDetector mGestureDetector;

        public interface OnItemClickListener {
            void onItemClick(View view, int position);
        }

        public RecyclerClickListener(Context context, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });
        }

        @Override public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildPosition(childView));
                return true;
            }
            return false;
        }

        @Override public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) { }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

    }
}

