package com.intentsolutions.tad.factorytest.messages;

/**
 * Created by tjmercer on 6/22/16.
 */
public class Response {
    private boolean mResult;

    public boolean getResult() {
        return mResult;
    }

    public void setResult(boolean result) {
        mResult = result;
    }
}
