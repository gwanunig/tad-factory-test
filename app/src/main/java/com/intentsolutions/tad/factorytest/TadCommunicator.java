package com.intentsolutions.tad.factorytest;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import com.intentsolutions.tad.factorytest.messages.Response;
import com.squareup.otto.Bus;

import com.intentsolutions.tad.factorytest.messages.BatteryResponse;
import com.intentsolutions.tad.factorytest.messages.BuzzerResponse;
import com.intentsolutions.tad.factorytest.messages.ChargeResponse;
import com.intentsolutions.tad.factorytest.messages.FingerEnrollmentResponse;
import com.intentsolutions.tad.factorytest.messages.FingerPrintResponse;
import com.intentsolutions.tad.factorytest.messages.FingerSensorResponse;
import com.intentsolutions.tad.factorytest.messages.LedBlueResponse;
import com.intentsolutions.tad.factorytest.messages.LedGreenResponse;
import com.intentsolutions.tad.factorytest.messages.LedRedResponse;
import com.intentsolutions.tad.factorytest.messages.MotorResponse;
import com.intentsolutions.tad.factorytest.messages.MuteButtonResponse;
import com.intentsolutions.tad.factorytest.messages.PillResponse;
import com.intentsolutions.tad.factorytest.messages.PronePositionResponse;
import com.intentsolutions.tad.factorytest.messages.UprightPositionResponse;
import com.intentsolutions.tad.factorytest.messages.VibrateResponse;

// This class is a bus publisher.  It publishes events to the bus using the "post" method of the
// mBus property.  The mBus property is the bus used appication wide.  This is important.  The
// base class for activities inherits from BaseActivity and it, too, subscribes to the applicaiton
// bus.  An app can have more than one bus just like an iOS app can have more than one
// NSNotificationCenter.
// When setLedsBlue is invoked, it publishes a message containing a result to the bus.


// TODO: Figure out why otto cannot be run on the main thread?
// Caused by: java.lang.IllegalStateException: Event bus [Bus "default"] accessed from non-main thread null
public class TadCommunicator {

	private static final String TAG = "TadCommunicator";
	private final Bus mBus;
	public BluetoothDevice mTadDevice;

	public TadCommunicator(TestDisplayActivity testDisplayActivity) {
		mBus = TadCommunicatorApp.getInstance().getBus();
		mTadDevice = null;
	}

	public void setLedsRed() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setLedsBlue() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setLedsGreen() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setMute() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setProne() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setUpright() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setBuzzer() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setVibrate() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setCharge() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setBattery() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setMotor() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setFingerSensor() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setFingerEnrollment() {
		Log.e(TAG, "Method has not been implemented");
		Response R = new Response();
		R.setResult(true);
		mBus.post(R);
	}

	public void setFingerRead() {
		Log.e(TAG, "Method has not been implemented");
		FingerPrintResponse lbr = new FingerPrintResponse();
		lbr.setResult(true);
		mBus.post(lbr);
	}

	public void setPillDelivery() {
		Log.e(TAG, "Method has not been implemented");
		PillResponse lbr = new PillResponse();
		lbr.setResult(true);
		mBus.post(lbr);
	}

}
