package com.intentsolutions.tad.factorytest;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.intentsolutions.tad.factorytest.messages.BatteryResponse;
import com.intentsolutions.tad.factorytest.messages.BuzzerResponse;
import com.intentsolutions.tad.factorytest.messages.ChargeResponse;
import com.intentsolutions.tad.factorytest.messages.FingerEnrollmentResponse;
import com.intentsolutions.tad.factorytest.messages.FingerPrintResponse;
import com.intentsolutions.tad.factorytest.messages.FingerSensorResponse;
import com.intentsolutions.tad.factorytest.messages.LedBlueResponse;
import com.intentsolutions.tad.factorytest.messages.LedGreenResponse;
import com.intentsolutions.tad.factorytest.messages.LedRedResponse;
import com.intentsolutions.tad.factorytest.messages.MotorResponse;
import com.intentsolutions.tad.factorytest.messages.MuteButtonResponse;
import com.intentsolutions.tad.factorytest.messages.PillResponse;
import com.intentsolutions.tad.factorytest.messages.PronePositionResponse;
import com.intentsolutions.tad.factorytest.messages.UprightPositionResponse;
import com.intentsolutions.tad.factorytest.messages.VibrateResponse;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * This class provides interaction with the user for conducting tests after a TAD is selected
 * from the {@link TadSearchActivity}. <ul>
 * <li>TODO: Modify the screen to show the test prompt and test result, removing the dialog.</li>
 * <li>TODO: Modify the screen to have two buttons.</li>
 * <li>TODO: Only show one button before the exection of a test and label it start/continue.</li>
 * <li>TODO: Show two buttons when you're asking for test execution results: Yes and No</li>
 * <li>TODO: If the user says pass, move to the next test by showing the initial prompt and displaying
 *       a single button.</li>
 * <li>TODO: If the user says fail, abort the tests and provide some feedback to them letting them no
 *       the device did NOT pass before returning them to the {@link TadSearchActivity} by invoking
 *       {@link #finish()}. </li></ul>
 */

/*
	interface TestDisplayActivity : BaseActivity<FactoryTestRunnerListener>
*/
public class TestDisplayActivity extends BaseActivity implements FactoryTestRunnerListener {
    public static final int BLUETOOTH_TAD_DEVICE = 1;

    private TadCommunicator mTad;
    private TextView mTitleView;
    private TextView mPromptView;
    private Button   mContinueButton;
    private Button   mCancelButton;

    private static final String TAG = "TestDisplayActivity";

    private BluetoothDevice mTadDevice;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_display);
        mTad = new TadCommunicator(this);

        mTitleView = (TextView) findViewById(R.id.title_view);
        mPromptView = (TextView) findViewById(R.id.prompt_view);
        mContinueButton = (Button) findViewById(R.id.continue_button);
        mCancelButton = (Button) findViewById(R.id.cancel_button);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testFailed();
            }
        });

        //Method called for testing purposes only since I have an empty recyclerView.
//        pretestRedLED();
        boolean didAllTestsPass = new FactoryTestRunner(this).runTests(

                new FactoryTest(getResources().getString(R.string.title_red),
                        getResources().getString(R.string.pretest_red),
                        getResources().getString(R.string.test_pass_red),
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setLedsRed();
                                Log.d(TAG, "Red");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_blue),
                        getResources().getString(R.string.pretest_blue),
                        getResources().getString(R.string.test_pass_blue),
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setLedsBlue();
                                Log.d(TAG, "Blue");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_green),
                        getResources().getString(R.string.pretest_green),
                        getResources().getString(R.string.test_pass_green),
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setLedsGreen();
                                Log.d(TAG, "Green");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_mute),
                        getResources().getString(R.string.pretest_mute),
                        "",
                        new Runnable() {
                            @Override
                            public void run() {
						        mTad.setMute();
                                Log.d(TAG, "Mute");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_prone),
                        getResources().getString(R.string.pretest_prone),
                        "",
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setProne();
                                Log.d(TAG, "Lay down");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_upright),
                        getResources().getString(R.string.pretest_upright),
                        "",
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setUpright();
                                Log.d(TAG, "Upright");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_buzzer),
                        getResources().getString(R.string.pretest_buzzer),
                        getResources().getString(R.string.test_pass_buzzer),
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setBuzzer();
                                Log.d(TAG, "Buzzer");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_vibrate),
                        getResources().getString(R.string.pretest_vibrate),
                        getResources().getString(R.string.test_pass_vibrate),
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setVibrate();
                                Log.d(TAG, "Vibrate");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_battery),
                        getResources().getString(R.string.pretest_battery),
                        "",
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setBattery();
                                Log.d(TAG, "Battery");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_charge),
                        getResources().getString(R.string.pretest_charge),
                        "",
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setCharge();
                                Log.d(TAG, "Charge");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_motor),
                        getResources().getString(R.string.pretest_motor),
                        getResources().getString(R.string.test_pass_motor),
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setMotor();
                                Log.d(TAG, "Motor");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_finger),
                        getResources().getString(R.string.pretest_finger_sensor),
                        "",
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setFingerSensor();
                                Log.d(TAG, "Sensor");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_finger),
                        getResources().getString(R.string.pretest_finger_enrollment),
                        "",
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setFingerEnrollment();
                                Log.d(TAG, "Enroll");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_finger),
                        getResources().getString(R.string.pretest_finger_print),
                        "",
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setFingerRead();
                                Log.d(TAG, "Print");
                            }
                        }),

                new FactoryTest(getResources().getString(R.string.title_pill),
                        getResources().getString(R.string.pretest_pill),
                        getResources().getString(R.string.test_pass_pill),
                        new Runnable() {
                            @Override
                            public void run() {
                                mTad.setPillDelivery();
                                Log.d(TAG, "Pill");
                            }
                        })
        );
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BLUETOOTH_TAD_DEVICE) {
            if (resultCode == RESULT_OK) {
                // Set the name from the intent.
                mTadDevice = data.getParcelableExtra(TadSearchActivity.BLUE_TOOTH_TAD_DEVICE);
                mTad.mTadDevice = mTadDevice;
            }
        }
    }

    @Override
    public void onTestReady(final FactoryTestRunner runner, FactoryTest test) {
        Log.d(TAG, "Test ready:");
        Log.d(TAG, test.getInitialPrompt());
        mTitleView.setText(test.getPromptTitle());
        mPromptView.setText(test.getInitialPrompt());
        mCancelButton.setVisibility(View.INVISIBLE);
        // COuld be a dialog to prompt the user.  If they say yes, start the test.
        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runner.startTest();
            }
        });
    }

    @Override
    public void onTestStart(FactoryTest test) {
        Log.d(TAG, "Running test");
        mPromptView.setText(getResources().getString(R.string.test_running));
    }

    @Override
    public void onTestComplete(final FactoryTestRunner runner, FactoryTest test) {
        // Prompt it's complete and ask for the result.
        Log.d(TAG, "Test Finished.  Did it work?:");
        // NOw do the damned dialog asking them to say yes.
        Log.d(TAG, test.getResultsPrompt());
        if (test.getResultsPrompt().length() > 0){
            mPromptView.setText(test.getResultsPrompt());
            mContinueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    runner.moveToNextTest();
                }
            });
            mCancelButton.setVisibility(View.VISIBLE);
        }else runner.moveToNextTest();
    }

    @Override
    public void onEnd(FactoryTestRunner runner) {
        // FIN
        Log.d(TAG, "We have exhausted all of the tests.  Hooray!  Can I go home now?");
        finish();
    }

    void testFailed() {
        Log.e(TAG, "Method has not been implemented");
        finish();
    }
}
