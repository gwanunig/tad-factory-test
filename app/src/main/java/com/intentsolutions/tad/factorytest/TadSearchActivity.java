package com.intentsolutions.tad.factorytest;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.intentsolutions.tad.factorytest.data.TadInfo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * This class displays all TADs accessible by the app.
 * TODO: Add a recycler view with some bogus entries.
 * TODO: Upon bogus entry selection, navigate to {@link TestDisplayActivity}.  Make sure to pass
 *       the TAD device (bogus entry) to the activity somehow so it can talk to it.  The bogus
 *       entry represents a {@link android.bluetooth.BluetoothDevice} to which the tests can talk.
 * TODO: {@link TestDisplayActivity} will invoke it's {@link TestDisplayActivity#finish()} }
 *       method upon test completion to return to this screen.
 */
public class TadSearchActivity extends BaseActivity {
    public static final String BLUE_TOOTH_TAD_DEVICE = "bluetooth";

    private static final String TAG = "TadSearchActivity";

    private RecyclerView mRecyclerView;
    private static ArrayList<TadInfo> TADS = null;
    private TadDeviceAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tad_search);
        detectTADS();
    }

    void detectTADS() {
        fillRecyclerView();
    }

    void fillRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.tadListView);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        TADS = (ArrayList<TadInfo>) createList(5);

        mAdapter = new TadDeviceAdapter(TADS);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnItemTouchListener(
                new TadDeviceAdapter.RecyclerClickListener(this, new TadDeviceAdapter.RecyclerClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // STUB:
                        // The click on the item must be handled
                        TadInfo tad = TADS.get(position);
                        Intent transition = new Intent(TadSearchActivity.this, TestDisplayActivity.class);
                        transition.setAction(Intent.ACTION_PICK);
                        transition.putExtra(BLUE_TOOTH_TAD_DEVICE, tad.TadDevice);
                        setResult(RESULT_OK, transition);
                        startActivityForResult(transition,1);
                    }
                })
        );
    }

    private ArrayList<TadInfo> createList(int size) {
        BluetoothDevice bob = null;

        ArrayList<TadInfo> result = new ArrayList<TadInfo>();

        for (int i=1; i <= size; i++) {
            TadInfo ti = new TadInfo();
            ti.TadName = "Tad" + i;
            ti.TadDevice = bob;

            result.add(ti);

        }

        return result;
    }

}
