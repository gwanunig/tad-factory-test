package com.intentsolutions.tad.factorytest;

/**
 * Created by andy on 6/23/16.
 */
public interface FactoryTestRunnerListener { // Essentially a protocol.
	void onTestReady(FactoryTestRunner runner, FactoryTest test);
	void onTestStart(FactoryTest test);
	void onTestComplete(FactoryTestRunner runner, FactoryTest test);
	void onEnd(FactoryTestRunner runner);
}
