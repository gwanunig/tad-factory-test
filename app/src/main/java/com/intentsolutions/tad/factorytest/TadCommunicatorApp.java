package com.intentsolutions.tad.factorytest;

import android.app.Application;
import android.bluetooth.BluetoothDevice;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;


public class TadCommunicatorApp extends Application {
	private static TadCommunicatorApp _instance;
	private Bus mBus;
	private BluetoothDevice mTadDevice;

	public static TadCommunicatorApp getInstance() {
		return _instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		TadCommunicatorApp._instance = this;
		mBus = new Bus(ThreadEnforcer.ANY);
	}

	public Bus getBus() {
		return mBus;
	}
}
