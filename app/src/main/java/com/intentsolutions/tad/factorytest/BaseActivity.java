package com.intentsolutions.tad.factorytest;

import android.support.v7.app.AppCompatActivity;

import com.squareup.otto.Bus;

/**
 * Created by andy on 6/21/16.
 */
public class BaseActivity extends AppCompatActivity {

	public interface NextStep {
		void nextAction();
	}

	@Override
	protected void onResume() {
		super.onResume();
		getBus().register(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		getBus().unregister(this);
	}

	protected Bus getBus() {
		return TadCommunicatorApp.getInstance().getBus();
	}
}
